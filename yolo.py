from tqdm import tqdm
from tracker import Tracker
from typing import List
from ultralytics import YOLO

import cv2 as cv
import cvzone
import matplotlib.pyplot as plt
import pandas as pd

from const import OFFSET

model=YOLO('yolov8s.pt')

SKIP_FACTOR = 1
FRAME_SIZE = (1020, 500)

def resize(src: str, dst: str):
    cap = cv.VideoCapture(src)
    fps = cap.get(cv.CAP_PROP_FPS)
    frame_count = int(cap.get(cv.CAP_PROP_FRAME_COUNT))
    out = cv.VideoWriter(dst, cv.VideoWriter_fourcc(*"avc1"), fps / SKIP_FACTOR, FRAME_SIZE) #type: ignore

    pbar = tqdm(total=frame_count, desc="Resizing", unit="frames")

    while cap.isOpened():
        ret, frame = cap.read()
        if not ret:
            break

        frame = cv.resize(frame,FRAME_SIZE)

        out.write(frame)
        pbar.update(1)

    cap.release()
    out.release()

def write_floats_to_file(floats: List[float], filename: str) -> None:
    # Open the file in write mode
    with open(filename, 'w') as file:
        # Convert the list of floats to a comma-separated string
        float_string = ",\n".join(map(str, floats))
        # Write the string to the file
        file.write(float_string)

def process(src: str, dst: str):
    cap = cv.VideoCapture(src)
    w = int(cap.get(cv.CAP_PROP_FRAME_WIDTH))
    h = int(cap.get(cv.CAP_PROP_FRAME_HEIGHT))
    fps = cap.get(cv.CAP_PROP_FPS)
    frame_count = int(cap.get(cv.CAP_PROP_FRAME_COUNT))
    out = cv.VideoWriter(dst, cv.VideoWriter_fourcc(*"avc1"), fps / SKIP_FACTOR, FRAME_SIZE) #type: ignore

    my_file = open("coco.txt", "r")
    data = my_file.read()
    class_list = data.split("\n") 

    count=0

    cartracker=Tracker()
    bustracker=Tracker()
    trktracker=Tracker()
    mcltracker=Tracker()
    
    cy1=OFFSET-15
    cy2=OFFSET+15
    offset=8

    upcar={}
    downcar={}
    upbus={}
    downbus={}
    uptruck={}
    downtruck={}
    upmcycle={}
    downmcycle={}

    countercarup=[]
    countercardown=[]
    counterbusup=[]
    counterbusdown=[]
    countertruckup=[]
    countertruckdown=[]
    countermcycleup=[]
    countermcycledown=[]

    CARCLR = (0, 0, 255)
    BUSCLR = (255, 0, 0)
    TRKCLR = (0, 255, 255)
    MCLCLR = (255, 255, 0)

    pbar = tqdm(total=frame_count, desc="Detecting phase", unit="frames") #type: ignore

    confidence_scores: List[float] = []

    while cap.isOpened():
        ret,frame = cap.read()
        if not ret:
            break

        count += 1
        if count % SKIP_FACTOR != 0:
            pbar.update(1)
            continue
        
        frame=cv.resize(frame,(1020,500))

        results=model.predict(frame, verbose=False)
        a=results[0].boxes.data #type: ignore
        px=pd.DataFrame(a).astype("float") #type: ignore
        
        carlist=[]
        buslist=[]
        trklist=[]
        mcllist=[]

        for index,row in px.iterrows():

            x1=int(row[0])
            y1=int(row[1])
            x2=int(row[2])
            y2=int(row[3])
            cfd = float(row[4])
            d=int(row[5])
            c=class_list[d]

            if 'car' in c:
                carlist.append([x1,y1,x2,y2,cfd])
                confidence_scores.append(cfd)
                
            elif'bus' in c:
                buslist.append([x1,y1,x2,y2,cfd])
                confidence_scores.append(cfd)
                
            elif 'truck' in c:
                trklist.append([x1,y1,x2,y2,cfd])
                confidence_scores.append(cfd)

            elif 'motorcycle' in c:
                mcllist.append([x1,y1,x2,y2,cfd])
                confidence_scores.append(cfd)
                
        # car
        car_bbox_idx=cartracker.update(carlist)
        for bbox in car_bbox_idx:
            x3,y3,x4,y4,id1=bbox
            cx3=int(x3+x4)//2
            cy3=int(y3+y4)//2

            pcs = cartracker.confidence_scores[id1][-1]
        
            cv.circle(frame,(cx3,cy3),4,(255,0,0),-1)
            cv.rectangle(frame,(x3,y3),(x4,y4),CARCLR,2)
            cvzone.putTextRect(frame,f'car {pcs:.2f}',(x3,y3),1,1, (255, 255, 255),CARCLR)

            if cy1 < (cy3 + offset) and cy1 > (cy3-offset):
                upcar[id1] = (cx3, cy3)
            if id1 in upcar:
                if cy2 < (cy3 + offset) and cy2 > (cy3-offset):
                    if countercarup.count(id1) == 0:
                        countercarup.append(id1)

            if cy2 < (cy3 + offset) and cy2 > (cy3-offset):
                downcar[id1] = (cx3, cy3)
            if id1 in downcar:
                if cy1 < (cy3 + offset) and cy1 > (cy3-offset):
                    if countercardown.count(id1) == 0:
                        countercardown.append(id1)

        # bus
        bus_bbox_idx=bustracker.update(buslist)
        for bbox in bus_bbox_idx:               
            x3,y3,x4,y4,id1=bbox
            cx3=int(x3+x4)//2
            cy3=int(y3+y4)//2

            pcs = bustracker.confidence_scores[id1][-1]
        
            cv.circle(frame,(cx3,cy3),4,(255,0,0),-1)
            cv.rectangle(frame,(x3,y3),(x4,y4),BUSCLR,2)
            cvzone.putTextRect(frame,f'bus {pcs:.2f}',(x3,y3),1,1, (255, 255, 255),BUSCLR)

            if cy1 < (cy3 + offset) and cy1 > (cy3-offset):
                upbus[id1] = (cx3, cy3)
            if id1 in upbus:
                if cy2 < (cy3 + offset) and cy2 > (cy3-offset):
                    if counterbusup.count(id1) == 0:
                        counterbusup.append(id1)

            if cy2 < (cy3 + offset) and cy2 > (cy3-offset):
                downbus[id1] = (cx3, cy3)
            if id1 in downbus:
                if cy1 < (cy3 + offset) and cy1 > (cy3-offset):
                    if counterbusdown.count(id1) == 0:
                        counterbusdown.append(id1)

        # truck
        trk_bbox_idx=trktracker.update(trklist)
        for bbox in trk_bbox_idx:
            x3,y3,x4,y4,id1=bbox
            cx3=int(x3+x4)//2
            cy3=int(y3+y4)//2

            pcs = trktracker.confidence_scores[id1][-1]
        
            cv.circle(frame,(cx3,cy3),4,(255,0,0),-1)
            cv.rectangle(frame,(x3,y3),(x4,y4),TRKCLR,2)
            cvzone.putTextRect(frame,'truck',(x3,y3),1,1, (0, 0, 0),TRKCLR)

            if cy1 < (cy3 + offset) and cy1 > (cy3-offset):
                uptruck[id1] = (cx3, cy3)
            if id1 in uptruck:
                if cy2 < (cy3 + offset) and cy2 > (cy3-offset):
                    if countertruckup.count(id1) == 0:
                        countertruckup.append(id1)

            if cy2 < (cy3 + offset) and cy2 > (cy3-offset):
                downtruck[id1] = (cx3, cy3)
            if id1 in downtruck:
                if cy1 < (cy3 + offset) and cy1 > (cy3-offset):
                    if countertruckdown.count(id1) == 0:
                        countertruckdown.append(id1)

        # mcycle
        mcl_bbox_idx=mcltracker.update(mcllist)
        for bbox in mcl_bbox_idx:
            x3,y3,x4,y4,id1=bbox
            cx3=int(x3+x4)//2
            cy3=int(y3+y4)//2

            pcs = mcltracker.confidence_scores[id1][-1]
        
            cv.circle(frame,(cx3,cy3),4,(255,0,0),-1)
            cv.rectangle(frame,(x3,y3),(x4,y4),MCLCLR,2)
            cvzone.putTextRect(frame,f'mcycle {pcs:.2f}',(x3,y3),1,1, (0, 0, 0),MCLCLR)

            if cy1 < (cy3 + offset) and cy1 > (cy3-offset):
                upmcycle[id1] = (cx3, cy3)
            if id1 in upmcycle:
                if cy2 < (cy3 + offset) and cy2 > (cy3-offset):
                    if countermcycleup.count(id1) == 0:
                        countermcycleup.append(id1)

            if cy2 < (cy3 + offset) and cy2 > (cy3-offset):
                downmcycle[id1] = (cx3, cy3)
            if id1 in downmcycle:
                if cy1 < (cy3 + offset) and cy1 > (cy3-offset):
                    if countermcycledown.count(id1) == 0:
                        countermcycledown.append(id1)
                        
        cv.line(frame,(1,cy1),(1018,cy1),(0,255,0),2)
        cv.line(frame,(3,cy2),(1016,cy2),(0,0,255),2)
        
        cup=len(countercarup)
        cdown=len(countercardown)
        cvzone.putTextRect(frame,f'car go down: {cup}',(50,60),1,1, (255, 255, 255), CARCLR)
        cvzone.putTextRect(frame,f'car go up: {cdown}',(50,90),1,1, (255, 255, 255), CARCLR)

        bup=len(counterbusup)
        bdown=len(counterbusdown)
        cvzone.putTextRect(frame,f'bus go down: {bup}',(50,120),1,1, (255, 255, 255), BUSCLR)
        cvzone.putTextRect(frame,f'bus go up: {bdown}',(50,150),1,1, (255, 255, 255), BUSCLR)

        tup=len(countertruckup)
        tdown=len(countertruckdown)
        cvzone.putTextRect(frame,f'truck go down: {tup}',(50,180),1,1, (0, 0, 0), TRKCLR)
        cvzone.putTextRect(frame,f'truck go up: {tdown}',(50,210),1,1, (0, 0, 0), TRKCLR)

        mup=len(countermcycleup)
        mdown=len(countermcycledown)
        cvzone.putTextRect(frame,f'mcycle go down: {mup}',(50,240),1,1, (0, 0, 0), MCLCLR)
        cvzone.putTextRect(frame,f'mcycle go up: {mdown}',(50,270),1,1, (0, 0, 0), MCLCLR)
        
        out.write(frame)
        pbar.update(1)
    cap.release()
    out.release()

    caravgcfd = [sum(values) / len(values) for values in cartracker.confidence_scores.values()]
    busavgcfd = [sum(values) / len(values) for values in bustracker.confidence_scores.values()]
    trkavgcfd = [sum(values) / len(values) for values in trktracker.confidence_scores.values()]
    mclavgcfd = [sum(values) / len(values) for values in mcltracker.confidence_scores.values()]

    confidence_acc = caravgcfd + busavgcfd + trkavgcfd + mclavgcfd

    fig, ax = plt.subplots()

    ax.hist(confidence_scores, label="overall", bins=16)
    ax.hist(confidence_acc, label="per object avg.", bins=16)
    ax.set_xlabel('Confidence Score')
    ax.set_ylabel('Frequency')
    ax.legend(loc='upper right')
    ax.set_title('Distribution of Confidence Scores')
    
    if dst == "test.mp4":
        fig.savefig("test.png")
        plt.close(fig)
        write_floats_to_file(confidence_scores, "test.txt")
    else:
        fig.savefig(f"tmp/log/{dst.split('/')[-1]}.png")
        plt.close(fig)
        write_floats_to_file(confidence_scores, f"tmp/log/{dst.split('/')[-1]}.txt")

if __name__ == "__main__":
    import argparse

    parser = argparse.ArgumentParser("Test")
    parser.add_argument("--input", help="video file")

    args = parser.parse_args()

    process(args.input, "test.mp4")
