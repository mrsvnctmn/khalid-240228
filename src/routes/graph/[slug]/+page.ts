import type { PageLoad } from "./$types";

export const load: PageLoad = async ({ fetch, params }) => {
    const p = new URLSearchParams(params.slug);

    const formats = ["av1", "vvc"];
    const filters = ["false", "true"];

    const ff = formats.flatMap(x => filters.map(y => {
        return {
            format: x,
            filtered: y,
        }
    }));

    const fetchJsonForFormat = async (fmt: {format: string, filtered: string}) => {
        const response = await fetch("/api/graph", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                filename: p.get("filename"),
                filterenable: p.get("filterenable") === fmt.filtered,
                format: fmt.format,
            }),
        });
        return response.json();
    }

    const fetchPromise = ff.map(fetchJsonForFormat);

    const res = await Promise.all(fetchPromise);

    return {
        filename: p.get("filename"),
        result: res,
    };
};