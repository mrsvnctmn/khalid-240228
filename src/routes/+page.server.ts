import type { Actions } from "./$types";
import { writeFileSync, mkdirSync } from 'fs';

export const actions = {
    default: async ({request}) => {
        const formReq = await request.formData();

        const formData = Object.fromEntries(formReq);
        const { input_video } = formData as { input_video: File };

        mkdirSync("tmp/sources", { recursive: true });
        writeFileSync(`tmp/sources/${input_video.name}`, Buffer.from(await input_video.arrayBuffer()));

        return {
            success: true
        };
    },
} satisfies Actions;