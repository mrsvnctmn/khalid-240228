import { backend } from "$lib";
import type { RequestHandler } from "@sveltejs/kit";

export const POST: RequestHandler = async ({request}) => {
    const data = await request.json();

    const response = await fetch(`${backend}/data`, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
    });

    const json = await response.json();

    return new Response(JSON.stringify(json));
};