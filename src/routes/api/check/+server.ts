import { backend } from "$lib";
import type { RequestHandler } from "@sveltejs/kit";

export const POST: RequestHandler = async ({request}) => {
    const controller = new AbortController();
    setTimeout(() => controller.abort(), 5000);

    const data = await request.json();

    const response = await fetch(`${backend}/check`, {
        method: 'POST',
        headers: {
            "Content-Type": "application/json",
        },
        body: JSON.stringify(data),
        signal: controller.signal,
    });

    if (response.ok) {
        const json = await response.json();

        return new Response(JSON.stringify(json));
    } else {
        return new Response(JSON.stringify({error: "Not Found"}), {status: response.status});
    }
};