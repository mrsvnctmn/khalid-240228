// place files you want to import through the `$lib` alias in this folder.

export type Pair<T, K extends string, V> = {
    value: T;
} & { [P in K]: V}

export const backend = "http://localhost:24228";

export const qpOptions: Pair<number, "qp", number[]>[] = [
    {
        value: 0,
        qp: [22, 27]
    },
    {
        value: 1,
        qp: [27, 35]
    },
    {
        value: 2,
        qp: [32, 46]
    },
    {
        value: 3,
        qp: [37, 55]
    }
];

export type GraphData = {
    format: string,
    psnr: number,
    bitrate: number,
    qp: number,
    filtered: boolean,
    execution_time: number,
};