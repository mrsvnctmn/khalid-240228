$venvPath = ".venv"
$tmpPath = "tmp"

if (-not (Test-Path $tmpPath)) {
	Write-Host "Creating tmp folder..."
	
	New-Item -ItemType Directory -Path $tmpPath | Out-Null
	New-Item -ItemType Directory -Path "$tmpPath/sources" | Out-Null
	New-Item -ItemType Directory -Path "$tmpPath/processed" | Out-Null
	New-Item -ItemType Directory -Path "$tmpPath/database" | Out-Null
	New-Item -ItemType Directory -Path "$tmpPath/log" | Out-Null
}

if (-not (Test-Path $venvPath)) {
	Write-Host "Setting up python env..."
	
	& python -m venv .venv
	& ./.venv/Scripts/activate
	& pip install fastapi
	& pip install "uvicorn[standard]"
	& pip install jinja2
	& pip install aiofiles
	& pip install python-multipart
} else {
	& ./.venv/Scripts/activate
}

$lpath = $PSScriptRoot -replace '[\\/]', '/'

$env:PATH = $lpath + "/ffmpeg-pack-2;" + $env:PATH
