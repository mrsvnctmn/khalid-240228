import re

def get_average_psnr(line: str) -> float:
    # Define the regular expression pattern to match the "average" value
    pattern = r'average:(inf|[\d\.]+)'
    
    # Search for the pattern in the given line
    match = re.search(pattern, line)
    
    # If a match is found, extract the value and convert it to float
    if match:
        return float(match.group(1))
    else:
        raise ValueError(f"Average PSNR value not found in the line [{line}]")