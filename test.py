import argparse
import asyncio

from main import Format, Video, encode

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Original video tester")

    parser.add_argument("--input", help="Video file", required=True)
    parser.add_argument("--output", help="Video file", default="test_output.mp4")
    parser.add_argument("--filterenable", action="store_true", help="Enable filter", default=False)
    parser.add_argument("--detectionenable", action="store_true", help="Enable detection", default=False)
    parser.add_argument("--format", type=Format, choices=list(Format), default=Format.NONE, help="Encoding")

    args = parser.parse_args()

    normalized_input = args.input.replace("\\", "/")

    print(f"processing: {normalized_input}")

    source = Video(filename=normalized_input.split("/")[-1], format=args.format, qpselect=1, filterenable=args.filterenable, detectionenable=args.detectionenable)
    res = asyncio.run(encode(args.output, normalized_input, source), debug=True)

    if res is not None:
        psnr, bitr, exct = res
        print(f"psnr: {psnr:.3f}, bitr: {bitr:.3f} kbps, exct: {exct:.3f} ms")
    else:
        print(r"¯\_(ツ)_/¯")