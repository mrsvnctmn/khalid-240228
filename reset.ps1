# Get the directory where the script is located
$scriptDirectory = $PSScriptRoot

# Delete database
Remove-Item "$scriptDirectory/tmp/processed.db" -ErrorAction SilentlyContinue

# Delete all items inside "tmp/processed"
Remove-Item -Path "$scriptDirectory/tmp/processed/*" -Recurse -Force

# Delete all items inside "tmp/sources"
Remove-Item -Path "$scriptDirectory/tmp/sources/*" -Recurse -Force
