# uvicorn main:app --port 24228 & npm run preview -- --host
Start-Job -Name VCBackend -ScriptBlock { uvicorn main:app --port 24228 }
try {
    npm run preview -- --host
}
finally {
    Write-Host "Closing..."
    Stop-Job -Name VCBackend
}