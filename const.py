# yolo.py consts
OFFSET = 200 # default: 285, range = 0..500

# main.py consts
# urutan: F0 -> F1 -> F2
FILTER0 = "gblur=sigma=1.0"
FILTER1 = "eq=contrast=250:saturation=2.0"
FILTER2 = "unsharp=luma_msize_x=5:luma_msize_y=5:luma_amount=1.0"