from enum import Enum
from fastapi import FastAPI, HTTPException
from pydantic import BaseModel
from time import time_ns as timer

import asyncio
import os
import shutil
import subprocess

# import filterbank
import database
import utility
import yolo

from const import FILTER0, FILTER1, FILTER2

# new API
class Format(str, Enum):
    AV1 = "av1"
    VVC = "vvc"
    NONE = "none"

class Video(BaseModel):
    filename: str
    format: Format
    qpselect: int
    filterenable: bool
    detectionenable: bool

class Processed(BaseModel):
    filename: str

class GraphQuery(BaseModel):
    filename: str
    format: Format
    filterenable: bool

app = FastAPI()
fifo_queue = asyncio.Queue()
db = database.Database()

async def fifo_worker():
    print("starting ffmpeg worker...")
    while True:
        job, *args = await fifo_queue.get()
        await job(*args)

async def encode(processed_path: str, source_path: str, video: Video) -> None | tuple[float, float, float]:
    if db.get_video(processed_path) is not None:
        return

    start = timer()
    end = timer()

    filtered = "FILTER" if video.filterenable else "CLEAN"
    detected = "TRACKED" if video.detectionenable else "UNTRACKED"

    filename = f"{video.format}_{video.qpselect}_{filtered}_{detected}_{video.filename}"

    fmt = video.format
    flt = video.filterenable
    det = video.detectionenable

    temp_path = f"tmp/processed/t0_{filename}"
    temp_filtered_path = f"tmp/processed/tf_{filename}"

    # if not os.path.isfile(processed_path):
    if True:
        if fmt is Format.AV1:
            #inpute to av1
            subprocess.run([
                "ffmpeg",
                "-y",
                "-i",
                source_path,
                "-c:v",
                "libsvtav1",
                "-preset",
                "13",
                "-qp",
                f"{video.qpselect}",
                temp_path
                ], shell=True, text=True)

            end = timer()
            
            # filter stuff
            if flt:
                subprocess.run([
                    "ffmpeg",
                    "-y",
                    "-i", temp_path,
                    "-vf", f"{FILTER0},{FILTER1},{FILTER2}",
                    "-c:v", "libsvtav1",
                    temp_filtered_path
                ], shell=True, text=True)
            else:
                shutil.copyfile(temp_path, temp_filtered_path)

            if det:
                yolo.process(temp_filtered_path, processed_path)
            else:
                shutil.copyfile(temp_filtered_path, processed_path)
        elif fmt is Format.VVC:
            # input to vvc
            subprocess.run([
                "ffmpeg",
                "-y",       # allow overwrite
                "-i",       # input
                source_path,   
                "-c:v",     # encoder
                "libvvenc",
                "-qp",      # quant. param.
                f"{video.qpselect}",
                # "-tier",    #
                # "1",
                "-preset",  # 0 - 4 
                "0",
                temp_path
                ], shell=True, text=True)

            end = timer()

            # filter stuff & covert to av1
            if flt:
                subprocess.run([
                    "ffmpeg",
                    "-y",
                    "-i", temp_path,
                    "-vf", f"{FILTER0},{FILTER1},{FILTER2}",
                    "-c:v", "libsvtav1",
                    temp_filtered_path
                ], shell=True, text=True)
            else:
                subprocess.run([
                    "ffmpeg",
                    "-y",
                    "-i",
                    temp_path,
                    "-c:v",
                    "libsvtav1",
                    temp_filtered_path
                    ], shell=True, text=True)

            if det:
                yolo.process(temp_filtered_path, processed_path)
            else:
                shutil.copyfile(temp_filtered_path, processed_path)

        else:
            shutil.copyfile(source_path, temp_path)
            
            # filter stuff
            if flt:
                subprocess.run([
                    "ffmpeg",
                    "-y",
                    "-i", temp_path,
                    "-vf", f"{FILTER0},{FILTER1},{FILTER2}",
                    # "-c:v", "libsvtav1",
                    temp_filtered_path
                ], shell=True, text=True)
            else:
                shutil.copyfile(temp_path, temp_filtered_path)

            if det:
                yolo.process(temp_filtered_path, processed_path)
            else:
                shutil.copyfile(temp_filtered_path, processed_path)

        rpsnr = subprocess.run([
            "ffmpeg",
            "-i",
            source_path,
            "-i",
            temp_filtered_path,
            "-lavfi",
            "psnr",
            "-f",
            "null",
            "-"
        ], shell=True, text=True, capture_output=True)

        print(f"ERR: {rpsnr.stderr}")

        lines = rpsnr.stderr.splitlines()
        last = ""

        for line in reversed(lines):
            if line.strip():
                last = line
                break
        
        psnr = utility.get_average_psnr(last)

        rbitr = subprocess.run([
            "ffprobe",
            "-v", "error",
            "-select_streams", "v:0",
            "-show_entries", "stream=bit_rate",
            "-of", "default=noprint_wrappers=1:nokey=1",
            temp_filtered_path
        ], shell=True, text=True, capture_output=True)

        bitr = float(rbitr.stdout.strip()) / 1000

        exct = end - start

        if fmt is not Format.NONE:
            db.add_video(video.filename, video.format, source_path, processed_path, video.qpselect, flt, det, psnr, bitr, exct / (10 ** 6))

        return psnr, bitr, exct / (10 ** 6)

async def push_process(processed_path: str, source_path: str, video: Video):
    await fifo_queue.put((encode, processed_path, source_path, video))

@app.on_event("startup")
async def start_queue():
    asyncio.create_task(fifo_worker())

@app.post("/process", status_code=202)
async def process(video: Video):
    filtered = "FILTER" if video.filterenable else "CLEAN"
    detected = "TRACKED" if video.detectionenable else "UNTRACKED"
    filename = f"{video.format}_{video.qpselect}_{filtered}_{detected}_{video.filename}"
    source_path = f"tmp/sources/{video.filename}"

    if not os.path.isfile(source_path):
        raise HTTPException(status_code=404, detail=f"{video.filename} not found")

    processed_path = f"tmp/processed/{filename}"

    await push_process(processed_path, source_path, video)

    return {
        "src": processed_path,
    }

@app.post("/check")
async def check(processed: Processed):
    filename = processed.filename

    video = db.get_video(filename)

    if video is None:
        raise HTTPException(status_code=404, detail=f"Not found ({filename})")
    
    return video

@app.post("/data")
async def data(query: GraphQuery):
    return db.get_data(query.filename, query.format, query.filterenable)

@app.get("/debug")
async def debug():
    return db.debug()
