import sqlite3

class Database:
    def __init__(self):
        self.conn = sqlite3.connect('tmp/processed.db')
        self.conn.row_factory = sqlite3.Row
        self.init_db()

    def init_db(self):
        with self.conn:
            self.conn.execute("""
                CREATE TABLE IF NOT EXISTS processed (
                id INTEGER PRIMARY KEY AUTOINCREMENT,
                filename TEXT NOT NULL,
                format TEXT NOT NULL,
                source TEXT NOT NULL,
                destination TEXT NOT NULL,
                qp INTEGER NOT NULL,
                filtered INTEGER NOT NULL,
                detected INTEGER NOT NULL,
                psnr REAL NOT NULL,
                bitrate REAL NOT NULL,
                execution_time REAL NOT NULL)
            """)

    def add_video(self, filename: str, format: str, source: str, destination: str, qp: int, filtered: bool, detected: bool, psnr: float, bitrate: float, execution_time: float) -> int | None:
        f = 1 if filtered else 0
        d = 1 if detected else 0
        with self.conn:
            cursor = self.conn.execute("""
                INSERT INTO processed (filename, format, source, destination, qp, filtered, detected, psnr, bitrate, execution_time)
                VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
                """,
                (filename, format, source, destination, qp, f, d, psnr, bitrate, execution_time))

            return cursor.lastrowid

    def get_video(self, destination: str):
        cursor = self.conn.execute("SELECT * FROM processed WHERE destination = ?", (destination,))
        row = cursor.fetchone()
        return dict(row) if row else None
    
    def get_data(self, filename: str, format: str, filtered: bool):
        f = 1 if filtered else 0

        cursor = self.conn.execute("""
            SELECT format, psnr, bitrate, qp, filtered, execution_time 
            FROM processed
            WHERE filename = ? AND format = ? AND filtered = ? AND detected = 1
            ORDER BY bitrate
            """,
            (filename, format, f))

        res = cursor.fetchall()

        if res:
            return [dict(row) for row in res]
        else:
            cursor = self.conn.execute("""
                SELECT format, psnr, bitrate, qp, filtered, execution_time 
                FROM processed
                WHERE filename = ? AND format = ? AND filtered = ?
                ORDER BY bitrate
                """,
                (filename, format, f))
            
            return [dict(row) for row in cursor.fetchall()]
        
    
    def debug(self):
        cursor = self.conn.execute("SELECT * FROM processed")
        return [dict(row) for row in cursor.fetchall()]
    
