# AV1/VVC Encoder

## first time (baru download dari gitlab)
1. pastikan `node` dah keinstall ([NodeJS](https://nodejs.org/en/download/)).
    - cek instalasi pake `npm --version` di terminal.
2. jalanin command `npm i`

## cara jalanin (recomended)
1. buka 2 terminal, sama2 ke folder ini.
2. tiap terminal, run `./activate.ps1`.

> kalo baru pertama kali jalanin/downlaod dari gitlab, ini dulu siap step 1 barusan:
> `pip install opencv-python tqdm ultralytics cvzone matplotlib`

3. masing-masing terminal:
    - terminal 1: `npm run dev -- --host`
    - terminal 2: `uvicorn main:app --port 24228 --workers 4`
4. stop (?): `ctrl+c` dan/atau [stop](#stop).

## reset

buat ngehapus data2 video sementara (biar ga autoskip proses), jalanin `./reset.ps1`.

## stop

stop python: `taskkill /im python.exe /t /f`.

## check yolo original (`yolo.py`)

1. buka folder project
2. `./activate.ps1` (kalo belom)
3. copy file video ke folder project (misal `traffic.mp4`)
4. `python yolo.py --input traffic.mp4`
5. outputnya ntar `test.mp4` di folder project.
6. output data: `test.png` sama `test.txt`

## check proses original (`test.py`)

1. buka folder project
2. `./activate.ps1` (kalo belom)
3. copy file video ke folder project (misal `traffic.mp4`)
4. check cara pake: `python test.py --help`

> contoh mau filter dan output kustom:
> `python test.py --input traffic.mp4 --filterenable --output out.mp4`

5. output default: `test_output.mp4`
6. output data yolo kayak [di atas](#check-yolo-original-yolopy): di dir `tmp/log/` sesuai nama output


## ngatur posisi garis yolo
1. buka file `const.py`
2. ubah nilai variable `OFFSET`: default = 285, range = 0 (mentok atas) - 500 (bawah)

## ngatur filter
1. buka file `const.py`
2. ubah `FILTER0`-`2` secukupnya.

## histogram yolo
* buat histogram hasil upload di webnya ada di folder `temp/log`
* buat yang dari cara [cek original](#check-yolo-original) hasilnya `test.png`

## data cfd yolo
* buat histogram hasil upload di webnya ada di folder `temp/log`
* buat yang dari cara [cek original](#check-yolo-original) hasilnya `test.txt`